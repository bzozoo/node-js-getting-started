const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000
const TESZT = process.env.TESZTVAR || 'I CAN NOT FOUND TESZTVAR'
const UNDEFVAR = process.env.UNDEFV || 'I CANT FOUND UNDEFV'

express()
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => res.render('pages/index'))
  .listen(PORT, () => {
      console.log(`Hello in APP... ${ TESZT }`)
      console.log(`Teszt UNDEFVAR :::: ${ UNDEFVAR }`)
      console.log(`Hello in APP... Listening on ${ PORT }`)
   })
